# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

@loadDocuments = ()->
	$.ajax ({
	  type: 'GET',
	  async: false,
	  url: '/documents',
	  success:(data) ->
	  	if data.length > 0
	  		$('#Documents').html data
	  		$('#Documents li:first').trigger 'click' 
	  	else
	  		$('#Documents').html 'empty'
	    
	  error:(data) ->
	  	alert 'error ' + data
	})

	$.ajax ({
	  type: 'GET',
	  url: '/salts',
	  success:(data) ->
	  	if data.length > 0
	  		$('#Salt_docs').html data
	  		$('#Salt_docs li:first').trigger 'click' 
	  	else
	  		$('#Salt_docs').html 'empty'
	    
	  error:(data) ->
	  	alert 'error ' + data
	})