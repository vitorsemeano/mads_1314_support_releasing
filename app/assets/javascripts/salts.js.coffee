# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
@createSaltDocument = (evt)->
	if evt.keyCode == 13

		name = $('.new_doc:eq(1)').val()
		if name.length > 0

			$.ajax ({
		      type: 'POST',
		      url: '/salts',
		      data: { name : name },
		      success:(data) ->
                if data.length > 0
                    $('#Salt_docs').html data
                    $('#Salt_docs li:last').trigger 'click' 
                else
                    $('#Salt_docs').html 'empty'
		      error:(data) ->
		    })
		$('.new_doc:eq(1)').val 'New Document'
		$('.new_doc:eq(1)').blur()

@deleteSalt = (id)->
	$.ajax ({
      type: 'DELETE',
      url: '/salts/' + id,
      success:(data) ->
        $('#right .content').html ''
        $('#right .default_content').css 'display', 'block'
        loadDocuments()
      error:(data) ->
      	loadDocuments()
    })

@editSalt = (index,path,id)->

	$('#Salt_docs li').each (index,value)->
		$(this).removeClass 'selected'

	$('#Salt_docs li:eq(' + index + ')').addClass 'selected'


	$('#right .content').load path, (response,status,xhr)->
		if status != 'success'
			$('.default_content:eq(1)').css 'display', 'block'
		else
			$('.default_content:eq(1)').css 'display', 'none'

$ ->
	$('#Salt_docs').bind 'ajax:success', (evt,data,status,xhr)->
		$('#right .content').html data
		$('#right .default_content').css 'display', 'none'
		$('#right .btns.save').each (value)->
			$(this).css 'display', 'inline'

		