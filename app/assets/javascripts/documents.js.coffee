# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

@selectedDocument = -1
@wtf = 124

@createDocument = (evt)->
	if evt.keyCode == 13
		type = $('.menu:first option:selected').text()
		name = $('.new_doc:first').val()
		if name.length > 0
			if type == 'doc'
				tdoc = 0
			else
				tdoc = 1

			$.ajax ({
		      type: 'POST',
		      url: '/documents',
		      data: { type : tdoc, name : name },
		      success:(data) ->
                if data.length > 0
                    $('#Documents').html data
                    $('#Documents li:last').trigger 'click' 
                else
                    $('#Documents').html 'empty'
		      error:(data) ->
		      	alert 'error ' + data
		    })
		$('.new_doc:first').val 'New Document'
		$('.new_doc:first').blur()

@deleteDocument = (id)->
	$.ajax ({
      type: 'DELETE',
      url: '/documents/' + id,
      success:(data) ->
        $('#middle .content').html ''
        $('#middle .default_content').css 'display', 'block'
        loadDocuments()
      error:(data) ->
      	loadDocuments()
    })


@editDocument = (index,path,id)->
	$('#Documents li').each (index,value)->
		$(this).removeClass 'selected'

	$('#Documents li:eq(' + index + ')').addClass 'selected'

	$('#middle .content').load path, (response,status,xhr)->
		if status != 'success'
			$('.default_content:first').css 'display', 'block'
		else
			$('.default_content:first').css 'display', 'none'




$ ->
	$('#Documents').bind 'ajax:success', (evt,data,status,xhr)->
		$('#middle .content').html data
		$('#middle .default_content').css 'display', 'none'
		$('#middle .btns.save').each (value)->
			$(this).css 'display', 'inline'

		$('#middle .content').show()
		$('#middle .content iframe').css 'visibility', 'visible'
		

	$('#Documents').bind 'ajax:before', (evt, xhr, settings)->
		$('#middle .content').hide()