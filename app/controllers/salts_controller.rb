class SaltsController < ApplicationController

  def verify
  end

  def download
  end

  def create
    doc = ReleaseSalt.new
    doc.content = ""
    doc.name = params[:name]
    doc.save
    redirect_to salts_path
  end

  def update
    doc = ReleaseSalt.find(params[:id])
    doc.content = params[:content]
    doc.save
    redirect_to edit_salt_path(params[:id])
  end

  def edit
    @doc = ReleaseSalt.find(params[:id])
    render layout: false
  end

  def destroy
    ReleaseSalt.find(params[:id]).delete
    render nothing: true
  end

  def index
    @docs = ReleaseSalt.all()
    render layout: false
  end
end
