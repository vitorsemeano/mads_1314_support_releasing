class DocumentsController < ApplicationController
  def create
    doc = ReleaseDocument.new
    doc.tdoc = params[:type]
    doc.content = ""
    doc.name = params[:name]
    doc.save
    redirect_to documents_path
  end

  def update
    doc = ReleaseDocument.find(params[:id])
    doc.content = params[:content]
    doc.save
    redirect_to edit_document_path(params[:id])
  end

  def edit
    @doc = ReleaseDocument.find(params[:id])
    render layout: false
  end

  def destroy
    ReleaseDocument.find(params[:id]).delete
    render nothing: true
  end

  def index
    @docs = ReleaseDocument.all()
    render layout: false
  end
end
