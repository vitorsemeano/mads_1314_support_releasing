class ReleaseSalt < ActiveRecord::Base
  attr_accessible :content, :name

  validates_uniqueness_of :name
end
