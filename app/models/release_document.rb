class ReleaseDocument < ActiveRecord::Base
  attr_accessible :content, :name, :tdoc

  validates_uniqueness_of :name
end
