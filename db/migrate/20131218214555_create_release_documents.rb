class CreateReleaseDocuments < ActiveRecord::Migration
  def change
    create_table :release_documents do |t|
      t.string :name
      t.integer :tdoc
      t.text :content
      
      t.timestamps
    end
  end
end