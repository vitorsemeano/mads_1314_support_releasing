class CreateReleaseSalts < ActiveRecord::Migration
  def change
    create_table :release_salts do |t|
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
